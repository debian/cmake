Source: cmake
Section: devel
Priority: optional
Maintainer: Kitware Debian Maintainers <debian@kitware.com>
Uploaders: Kyle Edwards <kyle.edwards@kitware.com>,
           Brad King <brad.king@kitware.com>,
           Kitware Robot <kwrobot@kitware.com>
Build-Depends: debhelper (>= 9~),
               freebsd-glue [kfreebsd-any],
               libncurses5-dev,
               procps [!hurd-any],
               libssl-dev,
               qtbase5-dev <!stage1>
Rules-Requires-Root: no
Standards-Version: 4.1.3
Vcs-Git: https://gitlab.kitware.com/debian/cmake.git
Vcs-Browser: https://gitlab.kitware.com/debian/cmake
Homepage: https://cmake.org/

Package: cmake
Architecture: any
Multi-Arch: foreign
Depends: cmake-data (= ${source:Version}),
         procps [!hurd-any],
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: gcc, make
Suggests: ninja-build
Description: cross-platform, open-source make system
 CMake is used to control the software compilation process using
 simple platform and compiler independent configuration files. CMake
 generates native makefiles and workspaces that can be used in the
 compiler environment of your choice. CMake is quite sophisticated: it
 is possible to support complex environments requiring system
 configuration, pre-processor generation, code generation, and template
 instantiation.
 .
 CMake was developed by Kitware as part of the NLM Insight
 Segmentation and Registration Toolkit project. The ASCI VIEWS project
 also provided support in the context of their parallel computation
 environment. Other sponsors include the Insight, VTK, and VXL open
 source software communities.

Package: cmake-data
Architecture: all
Depends: ${misc:Depends}
Description: CMake data files (modules, templates and documentation)
 This package provides CMake architecture independent data files (modules,
 templates, documentation etc.). Unless you have cmake installed, you probably
 do not need this package.

Package: cmake-curses-gui
Architecture: any
Depends: cmake (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: curses based user interface for CMake (ccmake)
 CMake is used to control the software compilation process using simple
 platform and compiler independent configuration files. CMake generates native
 makefiles and workspaces that can be used in the compiler environment of your
 choice.
 .
 This package provides the CMake curses interface. Project configuration
 settings may be specified interactively through this GUI. Brief instructions
 are provided at the bottom of the terminal when the program is running. The
 main executable file for this GUI is "ccmake".

Package: cmake-qt-gui
Architecture: any
Build-Profiles: <!stage1>
Depends: cmake (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Provides: cmake-gui
Description: Qt based user interface for CMake (cmake-gui)
 CMake is used to control the software compilation process using simple
 platform and compiler independent configuration files. CMake generates native
 makefiles and workspaces that can be used in the compiler environment of your
 choice.
 .
 This package provides the CMake Qt based GUI. Project configuration
 settings may be specified interactively. Brief instructions are provided at
 the bottom of the window when the program is running. The main executable
 file for this GUI is "cmake-gui".
